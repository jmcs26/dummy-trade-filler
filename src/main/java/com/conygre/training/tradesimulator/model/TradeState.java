package com.conygre.training.tradesimulator.model;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String tradeState;

    private TradeState(String tradeState) {
        this.tradeState = tradeState;
    }

    public String getState() {
        return this.tradeState;
    } 
}
